from .event import Event2

class PetEvent(Event2):
    NAME = "pet"

    def perform(self):
        if not self.object.has_prop("petable") :
            self.fail()
            return self.inform("pet.failed")
        self.inform("pet")
