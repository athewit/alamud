from .event import Event3

class StartWithEvent(Event3):
    NAME = "start-with"

    def perform(self):
        if not self.object.has_prop("startable") or not self.object2.has_prop("startable"):
            self.fail()
            return self.inform("start-with.failed")
        self.inform("start-with")
