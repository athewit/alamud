from .action import Action3
from mud.events import StartWithEvent

class StartWithAction(Action3):
    EVENT = StartWithEvent
    ACTION = "start-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
