from .action import Action2
from mud.events import PetEvent

class PetAction(Action2):
    EVENT = PetEvent
    ACTION = "pet"
    RESOLVE_OBJECT = "resolve_for_use"
